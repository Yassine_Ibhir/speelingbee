package spellingbee.client;

import javafx.scene.control.Tab;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import spellingbee.network.Client;

/**
 * This class extends the Tab class and defines the frontend Score tab for the game. 
 * It also uses the Client to make a GUI with GridPane as its container.
 * @author David Pizzolongo
 */
public class ScoreTab extends Tab{
	private Client client;
	private Text[] categories = new Text[5];
	private Text[] points = new Text[5];
	private Text scoreText = new Text();
	private Text score = new Text();
	private GridPane gp = new GridPane();


	//array that will store the point brackets 
	private String[] brackets;

	/**
	 * This parameterized constructor sets the title of the tab to Score and the content to be the GridPane.
	 * It also calls various methods to create the Text objects to be displayed, add them to the GridPane
	 * and call createStyling to style them.  
	 * @param client Client object responsible for the game
	 * @author David
	 */
	public ScoreTab(Client client) {
		super("Score");
		this.client = client;
		createTextFields();
		createGridPane();
		createStyling();
		this.setContent(gp);
	}

	/**
	 * Getter method
	 * @return Client object
	 * @author David 
	 */
	public Client getClient() {
		return this.client;
	}

	/**
	 * This method retrieves the point thresholds from the server and makes new Text
	 * objects with the names of categories, the amount of points as well as the score (which starts at 0). 
	 * @author David 
	 */
	private void createTextFields() {
		String bracketStr = this.client.sendAndWaitMessage("getBrackets");
		String bracket = bracketStr.substring(1,bracketStr.length()-1);
		brackets = bracket.split(", ");
		categories[0] = new Text("Queen Bee");
		categories[1] = new Text("Genius");
		categories[2] = new Text("Amazing");
		categories[3] = new Text("Good");
		categories[4] = new Text("Getting Started");
		//points are displayed in decreasing order
		int index = 4;
		for(int i=0; i < points.length; i++) {
			points[i] = new Text(brackets[index]);
			index--;
		}
		scoreText.setText("Current Score");
		score.setText("0");
	}

	/**
	 * createStyling creates the basic styling of the GridPane, the categories and 
	 * the score field.
	 * @author David
	 */
	private void createStyling() {
		gp.setHgap(10);
		for(Text category:categories) {
			category.setFill(Color.GREY);
		}	
		score.setFill(Color.RED);
	}

	/**
	 * The method createGridPane adds the various Text objects to the grid (categories, points and score),
	 * incrementing the rows each time.  
	 * @author David
	 */
	private void createGridPane(){
		int row = 0;
		final int lastRow = 5;
		for(int i=0; i< categories.length; i++) {
			gp.add(categories[i], 0, row, 1, 1);
			gp.add(points[i], 1, row, 1, 1);
			row++;
		}
		gp.add(scoreText, 0, lastRow, 1, 1);
		gp.add(score, 1, lastRow, 1, 1);
	}

	/**
	 * The method refresh makes a request to the server to get the user's new score and
	 * sets the text object to display the new score. It also changes the color of achieved 
	 * milestones to black.  
	 * @author David 
	 */
	public void refresh() { 
		String currentScore = client.sendAndWaitMessage("getScore");
		score.setText(currentScore);
		for(int i=0; i< points.length; i++) {
			if(Integer.parseInt(currentScore) >= Integer.parseInt(points[i].getText())) {
				categories[i].setFill(Color.BLACK);
			}
		}
	}

	/**
	 * This method provides extra styles when the user gets a panagram.
	 * A new text object is displayed, congratulating the user.
	 * @author David
	 */
	public void extraStyling() {
		Text panagram = new Text("Congrats on the panagram!");
		panagram.setFill(Color.DARKGOLDENROD);
		//adds the text to a new row
		gp.add(panagram, 0, 6, 1, 2);
	}


}
