package spellingbee.client;


import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TabPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import spellingbee.events.*;
import spellingbee.network.*;


/**
 * The SpellingBeeClient class is where the GUI will be put together 
 * and displayed to the user. It listens to user events and reacts appropriately.
 * @author David Pizzolongo and Yassine Ibhir
 */
public class SpellingBeeClient extends Application {

	private Client client = new Client();

	private GameTab game = new GameTab(client);

	private ScoreTab score = new ScoreTab(client);

	/**
	 * @override This method overrides the Application start method.
	 * It creates a group containing a TabPane, which has two tabs. When called, it displays
	 * the scene on the stage(window).
	 * @author David Pizzolongo and Yassine Ibhir
	 */
	public void start(Stage stage) {

		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.WHITE);

		//associate scene to stage and show
		stage.setTitle("Spelling Bee"); 
		stage.setScene(scene); 
		stage.show();

		TabPane tp = new TabPane();

		game.setGame();

		tp.getTabs().addAll(game, score);

		root.getChildren().add(tp);

		waitingForEvents();

	}

	/**
	 *  method listens for events and setOnaction
	 *   with the appropriate class.
	 *  @author Yassine Ibhir and David Pizzolongo
	 */


	private  void waitingForEvents() {
		// setOnaction on every button inside the hbox.
		for(int i = 0; i < GameTab.getNumButtons() ; i++) {
			Button clicked = (Button) game.getHbox().getChildren().get(i);
			clicked.setOnAction(new LettersEvents(game.getWordField(),clicked));
		}
		// setOnaction on submit button
		game.getSubmit().setOnAction(new SubmitEvent(game));

		// setOnaction on clear button
		game.getClear().setOnAction(new ClearEvent(game.getWordField()));

		// setOnaction on delete button
		game.getDelete().setOnAction(new DeleteEvent(game.getWordField()));

		// set listener on the score field 
		game.getScoreField().textProperty().addListener(new ChangeListener<String>(){
			public void changed(ObservableValue<? extends String> scorefield, String oldscore, String newscore) {
				score.refresh(); 
				
				//finds the message the user got and if it is a panagram, it calls the extraStyling method
				Text result = (Text) game.getMessages().getChildren().get(0);
				if(result.getText().equalsIgnoreCase("panagram")){
					score.extraStyling();
				}
			}
		});
	}

	public static void main(String[] args) {
		Application.launch(args);
	}  
}
