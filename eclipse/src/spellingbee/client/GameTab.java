package spellingbee.client;

import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import spellingbee.network.Client;

/**
 * GameTab  extends the Tab class.
 * The GameTab object will hold elements
 * related to the game such us buttons..
 * @author Yassine Ibhir
 */
public class GameTab extends Tab  {
	
	private Client client;
	
	// the vbox  contains all elements of the gameTab
	private VBox container = new VBox();
	
	// the hbox contains the 7 buttons
	private HBox letters = new HBox();
	
	// texfield holds the word the user plays
	private  TextField word = new TextField();
	
	 //the hbox contains the 3 buttons(submit,clear,and delete)
	private HBox controlButtons = new HBox();
	
	// button to submit the word played
	private  Button submit = new Button("submit");
	
	// button to clear the letters of word
	private  Button clear = new Button("clear");
	
	// button to delete the letters of word
	private  Button delete = new Button("delete");
	
	// the hbox contains msg and score elements 10 is the space between children
	private HBox messages = new HBox(10);
	
	// msg to display to the user after submit
	private  Text msg = new Text("Start");
	
	// currentScore to display to the user after submit
	private  Text score = new Text("0");
	
	// number of buttons/letters.
	private static final int numOfButtons = 7;
	
	// constructor
	public  GameTab(Client client) {
		
		super("Game");
		
		this.client = client;
		
		// initializes 7 buttons and add them to the hbox
		for(int i = 0 ; i<numOfButtons ; i++) {
			Button l = new Button();
			letters.getChildren().addAll(l);
		}
		
		// HBox
		controlButtons.getChildren().addAll(submit,clear,delete);
		// HBox
		messages.getChildren().addAll(msg,score);
		// VBox
		container.getChildren().addAll(letters,word,controlButtons,messages);
		
		this.setContent(container);
				
	}
	
	/**
	 * getters
	 * @return fields
	 */
	public static int getNumButtons() {
		return numOfButtons;
	}
	
	public Client getClient() {
		return this.client;
	}
	
	public HBox getMessages() {
		return this.messages;
	}
	
	public TextField getWordField() {
		return this.word;
	}
	
	public HBox getHbox() {
		return this.letters;
	}
	
	public Button getSubmit() {
		return this.submit;
	}
	
	public Button getClear() {
		return this.clear;
	}
	
	public Button getDelete() {
		return this.delete;
	}


	public Text getScoreField() {
		return this.score;
	}
	
	
	/* This method will send a request to the client
	 * to get all letters and center letter then set the buttons
	 * texts to each letter.
	 * @author Yassine Ibhir
	 */
	
	public void setGame() {
		
		String letters = this.client.sendAndWaitMessage("getAll");
		
		String center = this.client.sendAndWaitMessage("getCenter");
		
		int centerPosition = letters.indexOf(center);
		
		for(int i = 0; i<numOfButtons ; i++) {
			
			Button b = (Button) this.letters.getChildren().get(i);
			
			String letter = letters.substring(i, i+1);
			
			if(i != centerPosition) {
				b.setText(letter);
			}
			
			else {
				//center letter
				b.setText(letter);
				b.setTextFill(Color.RED);
			}			
		}
	}
}

//leftPanel.getButton().setOnAction(
//		new EventHandler<ActionEvent> () {
//			window.setText("Display something else"); // call a method from the MainPane class
//		}
//t.textProperty().addListener(
//        new ChangeListener<String>() {
//        public void changed(ObservableValue<? extends String> observable, String old, String newV) 	{
//       	 System.out.println(old + " " + newV);
//        }
//        }
//        
//   );

