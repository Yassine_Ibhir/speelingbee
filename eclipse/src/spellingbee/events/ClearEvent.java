package spellingbee.events;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

/**
 * This class will handle the clear button.
 * It will remove the last letter from textField.
 * @author Yassine Ibhir
 */
public class ClearEvent implements EventHandler<ActionEvent>{

	private TextField word;
	
	public ClearEvent(TextField word) {
		this.word = word;
		
	}
	@Override
	public void handle(ActionEvent arg0) {
		
		String wordText = word.getText();
		if(wordText.length()>0) {
		String letterCleared = wordText.substring(0,wordText.length()-1);
		word.setText(letterCleared);
		}
		
	}

}
