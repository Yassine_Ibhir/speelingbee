package spellingbee.events;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 * This class will handle the buttons/letters.
 * It will add the text of the button the word.
 * @author Yassine Ibhir
 */
public class LettersEvents implements EventHandler<ActionEvent>{
	
	private TextField word;
	private Button l;
	
	public LettersEvents(TextField word,Button l) {
		this.word = word;
		this.l  = l;
	}

	@Override
	public void handle(ActionEvent arg0) {
		
		String m = word.getText()+l.getText(); 
		
		word.setText(m);
		
	}
}
