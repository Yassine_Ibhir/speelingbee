package spellingbee.events;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.text.Text;
import spellingbee.client.GameTab;

/**
 * This class will handle the submit button.
 * It will send the word the player played to the client
 * and use the return String from the server to update the
 * message and score. 
 * @author Yassine Ibhir
 */
public class SubmitEvent implements EventHandler<ActionEvent>{ 
	
	private GameTab game;
	
	public SubmitEvent(GameTab game) {
		this.game = game;
	}

	@Override
	public void handle(ActionEvent arg0) {
		
		String playedWord = game.getWordField().getText().toLowerCase();
		
		String result = game.getClient().sendAndWaitMessage(playedWord);
		
		String [] scoreNmessage = result.split(";");
		
		for(int i = 0; i<scoreNmessage.length ; i++) {
		
		Text word = (Text) game.getMessages().getChildren().get(i);
		
		word.setText(scoreNmessage[i]);
			
		}
		
		//RESET THE FIELD
		 game.getWordField().setText("");
		
	}

}
