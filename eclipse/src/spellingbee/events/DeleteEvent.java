package spellingbee.events;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;


/**
 * This class will handle the delete button.
 * It will remove the word from textField.
 * @author Yassine Ibhir
 */
public class DeleteEvent  implements EventHandler<ActionEvent>{

	private TextField word;
	
	public DeleteEvent (TextField word) {
		this.word = word;
	}
	
	@Override
	public void handle(ActionEvent arg0) {
		
		word.setText("");
		
	}

}
