package spellingbee.server;

/**
 * This interface is implemented by  2 versions
 * of spellingBee game objects. 
 * @author Yassine Ibhir
 *
 */
public interface ISpellingBeeGame {
	
	// returns the points of a given word
	public int getPointsForWord(String attempt);
	
	// validates the word and returns a message
	public 	String getMessage(String attempt);
	
	// returns all 7 letters of the spellingBee object
	public	String getAllLetters();
	
	//returns center letter
	public char getCenterLetter();
	
	//returns the currentScore
	public int getScore();
	
	// returns the points of each category
	public int [] getBrackets();
}
