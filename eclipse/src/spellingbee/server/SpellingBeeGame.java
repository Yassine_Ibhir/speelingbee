package spellingbee.server;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * This class defines the functionality and the server side of the SpellingBeeGame. 
 * @author David Pizzolongo
 */
public class SpellingBeeGame implements ISpellingBeeGame {
	private Random rand = new Random();
	private String letters;
	private char centerLetter;
	private int score;
	private int[] brackets = new int[5];

	private Set<String> foundWords = new HashSet<String>();
	private static Set<String> possibleWords = new HashSet<String>();

	/**
	 * One of the overloaded constructors. It randomly generates the letters and the center letter. 
	 * It sets possibleWords to initially be all words from the file, and then calls findMatchingWords()
	 * to find the valid words for the given letters.
	 * @throws IOException
	 * @author David Pizzolongo
	 */
	public SpellingBeeGame()  {
		try {
			letters = findRandomLetters("datafiles\\letterCombinations.txt");
		} catch (IOException e) {
			e.printStackTrace();
		}
		centerLetter = letters.charAt(rand.nextInt(letters.length()));
		score = 0;
		possibleWords = createWordsFromFile("datafiles\\english.txt");
		possibleWords = findMatchingWords();
	}

	//parameterized constructor, taking as input a String of 7 already defined characters
	public SpellingBeeGame(String playboard) throws IOException {
		letters = playboard;
		centerLetter = letters.charAt(rand.nextInt(letters.length()));
		possibleWords = createWordsFromFile("datafiles\\english.txt");
		possibleWords = findMatchingWords();
	}

	/**
	 * This method adds to a HashSet all the words from the English dictionary and converts them to lowercase.
	 * If the file does not exist, it catches the exception and prints an appropriate message.
	 * @param path to the dictionary file 
	 * @return possibleWords is the Set containing the lines of the file
	 * @author David Pizzolongo
	 */
	private Set<String> createWordsFromFile(String path){
		Path p = Paths.get(path);
		List<String> lines = new ArrayList<String>();
		try {
			lines = Files.readAllLines(p); 
		}
		catch(IOException e){
			System.out.println("File is invalid");
		}
		for(String line:lines) {
			possibleWords.add(line.toLowerCase());
		}
		return possibleWords;
	}

	/**
	 * The method findRandomLetters retrieves a random line from the file provided that corresponds to 
	 * the 7 letters that will be used in the gameboard. It uses an ArrayList to store them.    
	 * @param path to the letter combinations file
	 * @return combinations is a String containing the random letters
	 * @throws IOException 
	 * @author David Pizzolongo
	 */
	private String findRandomLetters(String path) throws IOException {
		Set<String> set = createWordsFromFile(path);
		ArrayList<String> combinations = new ArrayList<String>(set);
		int num = rand.nextInt(set.size()); 
		return combinations.get(num);
	}

	//the getters
	
	/**
	 * getPointsForWord takes as input the user-entered word and calculates the number of points
	 * the user gets based on the SpellingBee rules (the minLength sets the boundary). 
	 * It also checks if the user entered a panagram and if so, they earn the amount of points for the length
	 * of the word, as well as an extra 7 points. 
	 * @override Implements the getPointsForWord method from the interface
	 * @param attempt is the user's attempt
	 * @author David Pizzolongo
	 */
	public int getPointsForWord(String attempt) {
		final int minLength = 4;
		if(attempt.length() == minLength) {
			return 1;
		}
		else if(isPanagram(attempt)) {
			return attempt.length() + 7;
		}
		else if(attempt.length() > minLength) {
			return attempt.length();
		}
		return 0;
	}

	/**
	 * The method determines the appropriate message based on if the attempt is valid 
	 * and the total points the user earned.
	 * @override Overrides getMessage() from the ISpellingBeeGame
	 * @param attempt represents the user's word
	 * @return String representing the message that will be displayed to the user
	 * @author David
	 */
	public String getMessage(String attempt) {
		int points = getPointsForWord(attempt);
		if(possibleWords.contains(attempt)) {
			if(!(foundWords.contains(attempt))) {
				foundWords.add(attempt);
				score = score + points;
				if(points > attempt.length()) {
					return "panagram";
				}
				else if(points > 0){
					return "good";
				}
			}
			else {
				return "already chosen";
			}
		}
		return "not good";
	}

	@Override
	public String getAllLetters() {
		return this.letters;
	}
	@Override
	public char getCenterLetter() {
		return this.centerLetter;
	}
	@Override
	public int getScore() {
		return this.score;
	}

	/**
	 * This method computes the maximum possible points that the user can get from one game (using valid words). 
	 * They are divided between rankings, each showing a different percentage of the points.
	 * @override getBrackets() method from the interface
	 * @return brackets is the array of the different levels 
	 * @author David
	 */
	public int[] getBrackets() {
		int maxPoints = 0;
		for(String word : possibleWords) {
			maxPoints += getPointsForWord(word);
		}

		//rankings of the game 
		brackets[0] = (int) (maxPoints * 0.25);
		brackets[1] = (int) (maxPoints * 0.50);
		brackets[2] = (int) (maxPoints * 0.75);
		brackets[3] = (int) (maxPoints * 0.90);
		brackets[4] = maxPoints;
		return this.brackets;
	}

	/**
	 * This method determines if the user used all 7 of the letters provided, returns true if so. 
	 * It also helps to determine the maximum possible points the user can get.
	 * @param word is the user's word or a word from the possibleWords set
	 * @return boolean value to represent if the word is a panagram
	 * @author David
	 */
	private boolean isPanagram(String word) {
		for(int i=0; i< letters.length(); i++) {
			if(word.indexOf(letters.charAt(i)) < 0){
				return false;
			}
		}
		return true;
	}

	/**
	 * This method retrieves the old value of possibleWords
	 * and creates a new HashSet that contains words that only have the letters on the gameboard and 
	 * absolutely have the center letter.
	 * @author David Pizzolongo and Yassine Ibhir
	 * @return validWords is the HashSet representing the new value for possibleWords
	 */
	private Set<String> findMatchingWords() {
		Set<String> validWords = new HashSet<String>();
		for(String s : possibleWords) {
			if(s.matches("^[" + letters + "]+$") && s.indexOf(centerLetter) > -1) {
				validWords.add(s);
			}
		}
		return validWords;
	}
}
