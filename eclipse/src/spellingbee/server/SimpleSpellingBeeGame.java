package spellingbee.server;

import java.util.HashSet;
import java.util.Set;

import spellingbee.client.GameTab;

/**
 * This class is a simple version of the back end
 * of SpeelingBee game. It has the same rules as the original game
 * but we hard code all the fields instead of reading from files.
 * @author Yassine Ibhir
 *
 */
public class SimpleSpellingBeeGame implements ISpellingBeeGame{

	// letters/buttons
	private String allLetters;

	//the required letter in the word
	private char centerLetter;

	// points categories of all possible words.
	private int[] brackets;

	//currentScore
	private int score = 0;

	// all possible words of the 7 and center letters
	private static Set <String> possibleWords = new HashSet <String>();

	// words already played
	private Set <String> wordsPlayed = new HashSet <String>();

	public SimpleSpellingBeeGame()  {
		this.allLetters = "wabojne";
		this.centerLetter = 'a';
		possibleWords.add("bonejaw");
		possibleWords.add("bean");
		possibleWords.add("wean");
		possibleWords.add("banjo");
		brackets = new int [] {5,10,15,18,20};

	}
	
	// Interface methods.
	
	/**
	 *  method returns 1 if word's length is 4
	 *  otherwise returns the length of the word
	 * @param attempt 
	 * @return int points for word
	 */
	@Override
	public int getPointsForWord(String attempt) {

		final int  fourLetterPoint = 1;

		final int  minLength= 4;

		if(attempt.length() == minLength) {
			return fourLetterPoint;
		}
		return attempt.length();
	}

	
	/**
	 *  method checks if the word is a panagram, 
	 *  already chosen, good or not good. 
	 *  It uses helper method isPanagram and contains method
	 *  of the collection to determine the message
	 * @param attempt 
	 * @return  String message to display.
	 */
	@Override
	public String getMessage(String attempt) {
		
		final int panagramBonus = 7;

		if(possibleWords.contains(attempt)) {

			if(!(wordsPlayed.contains(attempt))) {

				wordsPlayed.add(attempt);

				if(isPanagram(attempt)) {
					score = score + getPointsForWord(attempt)+panagramBonus;
					return "Panagram";
				}
				else {
					score = score + getPointsForWord(attempt);
					return "Good";
				}

			}
			else {
				return "Already chosen";
			}
		}

		return "Not good";

	}

	/**
	 *  method returns the string letters of the object
	 * @return  String all letters to display in buttons.
	 */
	@Override
	public String getAllLetters() {
		return this.allLetters;
	}

	/**
	 *  method returns the letter that the player
	 *  must use to build the word.
	 * @return  char center letter.
	 */
	@Override
	public char getCenterLetter() {
		return this.centerLetter;
	}


	/**
	 *  method returns the the current score of the game
	 * @return  int score.
	 */
	@Override
	public int getScore() {
		return this.score;
	}

	/**
	 *  method returns an array of categorized points.
	 *  The array contains 25%,50%,80%,90%, and 100% of
	 *  the maximum points that can obtained
	 *  with the given letters.
	 * @return  int [] points.
	 */
	@Override
	public int[] getBrackets() {
		return brackets;
	}

	/**
	 *  method checks if the word played is a panagram
	 * @param attempt
	 * @return boolean
	 */

	private boolean isPanagram(String attempt) {

		for(int i = 0; i<allLetters.length(); i++) {

			if(attempt.indexOf(allLetters.charAt(i)) == -1) {
				return false;
			}
		}

		return true;
	}


}
